const crypto = require("crypto");
const fs = require("fs");
const path = require("path");
const urlJoin = require("url-join");
const fetch = require("node-fetch");
const request = require('request');
const util = require('util');
const compareVersions = require("compare-versions");
const Client = require("ssh2-sftp-client");
const childProcess = require("child_process");
const rimraf = require("rimraf");

//функиция шифрует файл сборки и заливает на сервер обновлений
exports.publish = (options = {}) => {
    try {
        options = {...{
                platform: "linux",
                projectDir: null,
                cryptoAlgorithm: "aes-256-cbc",
                key: ""
            },...options};
        console.log(options);

        let projectDir = (options.projectDir === null) ? path.resolve("") : options.projectDir;

        let packageJsonFileName = path.resolve(projectDir,"package.json");
        let configJsonFileName = path.resolve(projectDir,"config.json");

        if(!fs.existsSync(packageJsonFileName)) {
            console.error("Error finding package.json file: " + packageJsonFileName);
            return;
        }

        if(!fs.existsSync(configJsonFileName)) {
            console.error("Error finding config.json file: " + configJsonFileName);
            return;
        }

        let packageJson = null;
        try {
            packageJson = JSON.parse(fs.readFileSync(packageJsonFileName,'utf8'));
        } catch(ex) {
            console.error("Error reading package.json file");
            console.error(ex);
            return;
        }

        let configJson = null;
        try {
            configJson = JSON.parse(fs.readFileSync(configJsonFileName,'utf8'));
        } catch(ex) {
            console.error("Error reading config.json file");
            console.error(ex);
            return;
        }

        if(options.platform !== "linux" && options.platform !== "win32") {
            console.error("Unsupported platform " + options.platform + ". Available win32 and linux");
            return;
        }
        let buildPlatform = options.platform;
        if(buildPlatform === "win32") buildPlatform = "win";
        if(typeof packageJson.build === 'undefined' || typeof packageJson.build[buildPlatform] === 'undefined' || typeof packageJson.build[buildPlatform].target === 'undefined') {
            console.error("Undefined build target in package.json for platform " + buildPlatform);
            return;
        }

        let fileExt = null;
        if(options.platform === "linux") fileExt = packageJson.build[options.platform].target;
        if(options.platform === "win32") fileExt = "exe";

        let fileName = path.resolve(projectDir,"dist",packageJson.name + " " + packageJson.version +  "." + fileExt);
        let fileNameEnc = fileName + ".enc";
        let clearFileNameEnc = fileNameEnc.replace(/^.*[\\\/]/, '')

        if(!fs.existsSync(fileName)) {
            console.error("File: " + fileName + " not exist");
            return;
        }

        let cipher = crypto.createCipher(options.cryptoAlgorithm, options.key);
        let inputStream = fs.createReadStream(fileName);
        let outputStream = fs.createWriteStream(fileNameEnc);

        inputStream.pipe(cipher).pipe(outputStream);

        outputStream.on('finish', function() {
            console.log('Encrypted file written to disk!');

            if(typeof configJson.transport.host === 'undefined') {
                console.error("Don't set transport.host in config file");
                return;
            }
            if(typeof configJson.transport.username === 'undefined') {
                console.error("Don't set transport.username in config file");
                return;
            }
            if(typeof configJson.transport.password === 'undefined') {
                console.error("Don't set transport.password in config file");
                return;
            }
            if(typeof configJson.transport.remotePath === 'undefined') {
                console.error("Don't set transport.remotePath in config file");
                return;
            }
            if(typeof configJson.transport.remoteUrl === 'undefined') {
                console.error("Don't set transport.remoteUrl in config file");
                return;
            }

            let updateJsonFileName = path.join(configJson.transport.remotePath, "update.json").replace(/\\/g, '/');
            let updateRecord = {
                "readme": "The first version",
                "update": urlJoin(configJson.transport.remoteUrl, options.platform + "-x64-prod-v" + packageJson.version, clearFileNameEnc),
                "install": urlJoin(configJson.transport.remoteUrl, options.platform + "-x64-prod-v" + packageJson.version, clearFileNameEnc),
                "version": packageJson.version
            };
            let remotePath = path.join(configJson.transport.remotePath,options.platform + "-x64-prod-v" + packageJson.version).replace(/\\/g, '/');

            let sftp = new Client();
            sftp.connect({
                host: configJson.transport.host,
                port: 22,
                username: configJson.transport.username,
                password: configJson.transport.password
            })
                .then(() => {
                    console.log("Connected to " + configJson.transport.host);
                    return sftp.mkdir(remotePath);
                })
                .then((data) => {
                    console.log(data);
                    console.log("Deploying encrypted build...");
                    return sftp.put(fileNameEnc,path.join(remotePath,clearFileNameEnc).replace(/\\/g, '/'));
                })
                .then((data) => {
                    console.log(data);
                    return sftp.exists(updateJsonFileName);
                })
                .then((data) => {
                    if(data) {
                        sftp.get(updateJsonFileName)
                            .then((data) => {
                                let updateJson = JSON.parse(data.toString());
                                updateJson[options.platform + '-x64-prod'] = updateRecord;

                                return sftp.put(Buffer.from(JSON.stringify(updateJson,null,'\t')),updateJsonFileName);
                            })
                            .then((data) => {
                                console.log(data);
                                sftp.end();
                            })
                            .catch(ex => {
                                console.log("Error write update.json file");
                                console.log(ex);
                                sftp.end();
                            });
                    } else {
                        let updateJson = {
                            [options.platform + "-x64-prod"]: updateRecord
                        };

                        sftp.put(Buffer.from(JSON.stringify(updateJson,null,'\t')),updateJsonFileName)
                            .then((data) => {
                                console.log(data);
                                sftp.end();
                            })
                            .catch(ex => {
                                console.log("Error write update.json file");
                                console.log(ex);
                                sftp.end();
                            });
                    }
                })
                .catch(err => {
                    console.log("Error connection");
                    console.log(err);
                    sftp.end();
                });
        });
    } catch (ex) { console.error(ex); }
};

//--------------------------------------------------------------------------------------------------------

let updaterInfo = {
    updaterIntervalId: null
};

exports.checkUpdate = async (options) => {
    try {
        const {app} = require("electron").remote;

        let appVersion = app.getVersion();
        if(appVersion === 'undefined') {
            console.error("Undefined appVersion in checkUpdate");
            return false;
        }

        if(typeof options.remoteUrl === 'undefined') {
            console.error("Don't set options.remoteUrl for update");
            return false;
        }
        if(typeof options.platform === 'undefined') {
            console.error("Don't set options.platform for update");
            return false;
        }
        if(options.platform !== 'linux' && options.platform !== 'win32') {
            console.error("Unsupported platform " + options.platform + ". Available win32 and linux");
            return;
        }

        let updateJsonUrl = urlJoin(options.remoteUrl,"update.json");
        console.log(updateJsonUrl);

        let updateJsonContent = await fetch(updateJsonUrl);
        let updateJson = await updateJsonContent.json()

        let updateJsonSectionName = options.platform + "-x64-prod";
        if(typeof updateJson[updateJsonSectionName] === 'undefined' || typeof updateJson[updateJsonSectionName].version === 'undefined') return false;

        console.log(updateJson[updateJsonSectionName].version);
        console.log(appVersion);

        if(compareVersions.compare(updateJson[updateJsonSectionName].version,appVersion,">") > 0) {
            return updateJson[updateJsonSectionName];
        } else return false;
    } catch(ex) { console.error(ex); return false; }
};

exports.downloadFile = (url,fileName) => {
    try {
        return new Promise((resolve,reject) => {
            const file = fs.createWriteStream(fileName);
            const req = request.get({
                url,
                timeout: 1000 * 60 * 3 // 3 minutes
            }).on('response', function (response) {
                if (response.statusCode !== 200) {
                    //console.error("Error downloading file " + url + ". Status: " + response.statusCode);
                    return reject({
                        responseMessage: "Error downloading file " + url + ". Status: " + response.statusCode,
                        error: true
                    });
                }
                console.log('Downloading file ' + url + '...');
            }).on('end', function () {
                //console.log('File downloaded success ' + url);
                return resolve({
                    responseMessage: 'File downloaded success ' + url,
                    error: false
                });
            }).on('error', function(err) {
                //console.error('Download file: error', err);
                return reject({
                    responseMessage: 'Error downloading file ' + url,
                    error: true
                });
            }).pipe(file);
        });
    } catch (ex) {
        return new Promise((resolve,reject) => {
            return reject({
                responseMessage: ex,
                error: true
            })
        });
    }
};

//получить размер удаленного файла
exports.getRemoteFileSize = (url) => {
    return fetch(url, { method: 'HEAD' }).then(res => Number(res.headers.get('content-length')));
};

exports.getFileStatAsync = util.promisify(fs.stat);

exports.clearAppData = () => {
    try {
        const {app} = require("electron").remote;

        console.log('Clear App Data');
        return new Promise((resolve, reject) => {
            rimraf(app.getPath('userData'), (err) => {
                if (err) return reject(`Could not delete old file ${app.getPath('userData')}: ${err.message}`);
                resolve();
            });
        });
    } catch(ex) {
        console.error("Error clearing app data: " + ex);
        return new Promise((resolve, reject) => reject("Error clearing app data: " + ex));
    }
}

exports.reboot = () => {
    console.log('sudo reboot');
    if(process.platform === 'linux') return childProcess.execFileSync('sudo', ['reboot']);
    return null;
}

exports.killApp = () => {
    try {
        const {app} = require("electron").remote;
        console.log('sudo pkill -f ' + app.getName());
        return childProcess.execFileSync('sudo', ['pkill', '-f', app.getName()]);
    } catch (ex) {
        console.error("Error killing app: " + ex);
        return new Promise((resolve, reject) => reject("Error killing app: " + ex));
    }
}

exports.deleteApp = (fileName) => {
    return new Promise((resolve, reject) => {
        fs.unlink(fileName, (err) => {
            if (err) return reject(`Could not delete old file ${fileName}: ${err.message}`);
            resolve();
        });
    });
}

const sleep = delay => new Promise(res => setTimeout(res, delay));
exports.updateApp = (options) => {
    if((options.platform === "linux" && typeof process.env.APPIMAGE === 'undefined') || 
        (options.platform === "win32" && typeof process.env.PORTABLE_EXECUTABLE_FILE === 'undefined') || (options.platform !== "linux" && options.platform !== "win32"))
        return new Promise((resolve, reject) => resolve());

    const {app} = require("electron").remote;

    let appPath = (options.platform === "linux") ? process.env.APPIMAGE : process.env.PORTABLE_EXECUTABLE_FILE;

    return sleep(10 * 1000)
        .then(() => this.clearAppData())
        .then(() => this.deleteApp(appPath))
        .then(() => {
            return new Promise((resolve,reject) => {
                if(typeof options.onClearData === 'function') {
                    try {
                        options.onClearData();
                    } catch(ex) {
                        return reject("Could not execute onClearData(). err: " + ex);
                    }
                }
                return resolve();
            });
        })
        .then(() => {
            if ( process.platform !== 'linux' ) return app.exit(0);
            return this.reboot();
        })
        .catch((ex) => {
            console.error("Error updating app: " + ex);
            return reject("Error updating app: " + ex);
        });
}

exports.updaterStart = async (options = {}) => {
    try {
        options = {...{
                platform: "linux",
                update_interval: 300,
                remoteUrl: "",
                cryptoAlgorithm: "aes-256-cbc",
                key: "",
                onClearData: null
            },...options
        };

        let isUpdaterWork = false;

        const updateFunc = async () => {
            try {
                if(isUpdaterWork) return;
                isUpdaterWork = true;

                let updateInfo = await this.checkUpdate(options);
                if(updateInfo === false) {
                    isUpdaterWork = false;
                    return;
                }

                let appDir = (typeof process.env.PORTABLE_EXECUTABLE_DIR !== 'undefined') ? process.env.PORTABLE_EXECUTABLE_DIR : path.resolve("");
                let fileName = path.resolve(appDir,updateInfo.update.substring(updateInfo.update.lastIndexOf('/')+1));
                let res = await this.downloadFile(updateInfo.update,fileName);
                if(res.error) {
                    isUpdaterWork = false;
                    console.error(res.responseMessage);
                    return;
                }

                let remoteFileSize = await this.getRemoteFileSize(updateInfo.update);
                let localFileStat = await this.getFileStatAsync(fileName);
                if(remoteFileSize !== localFileStat.size) {
                    isUpdaterWork = false;
                    return;
                }

                let extPos = path.extname(fileName);
                if(extPos.indexOf("enc") > 0) {
                    newFileName = fileName.substr(0,fileName.length - 4);
                    console.log(newFileName);

                    let cipher = crypto.createDecipher(options.cryptoAlgorithm, options.key);
                    let inputStream = fs.createReadStream(fileName);
                    let outputStream = fs.createWriteStream(newFileName);

                    inputStream.pipe(cipher).pipe(outputStream).on('end',() => console.log("HERE"));

                    outputStream.on("finish",async () => {
                        await this.updateApp(options);
                    });
                } else {
                    await this.updateApp(options);
                }

                isUpdaterWork = false;
            } catch(ex) {
                console.error(ex);
                isUpdaterWork = false;
            }
        };

        if(updaterInfo.updaterIntervalId !== null) clearInterval(updaterInfo.updaterIntervalId);
        updaterInfo.updaterIntervalId = setInterval(async () => {
            await updateFunc();
        },options.update_interval*1000);

        await updateFunc();
    } catch(ex) { console.error(ex); }
};




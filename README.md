encrypted-electron-publisher: модуль node.js для загрузки шифрованной сборки приложения Electron на сервер обновления и выкачивания его на клиентскую машину  
===============================================================================

Установка
-------------------------------------------------------------------------------

В корне своего проекта Electron выполните команду: 

	npm i encrypted-electron-publisher
	
Публикация сборки на сервере обновлений		
-------------------------------------------------------------------------------

Подключить модуль:

	const publisher = require("encrypted-electron-publisher");

Запустить публикацию сборки на сервер обновлений:

    publisher.publish({
        platform: process.platform,     
        cryptoAlgorithm: "aes-256-cbc",
        projectDir: null,   
        key: "0000000000000000000000000000000000000000000000000000000000000000"
    }); 
    
Параметры:
 - platform: платформа сборки (поддерживается win32 и linux)
 - cryptoAlgorithm: алгоритм шифрования. Данные пакет использует модуль шифрования Crypto. Об его алгоритмах шифрования можно узнать по ссылке [https://nodejs.org/api/crypto.html](https://nodejs.org/api/crypto.html)   
 - key: ключ для шифрования сборки по методы AES256
 - projectDir: папка, содежращая собранный проект. Папка должна содеражть файл package.json с информацией о проекте, файл 
 config.json, содержащий информацию о сервере обновления и папку dist с собранным проектом. Если projectDir = null, то 
 используется текущая директория проекта 
  
 Пример файла config.json: 
    
    {
        "transport": {
            "module": "ssh",
            "host": "update-server-host",
            "username": "user",
            "password": "password",
            "remotePath": "/home/ubuntu/builds",
            "remoteUrl": "http://update-server-host/builds/"
        },            
        "updatesJsonUrl": ""http://update-server-host/builds/"updates.json"
    }      
    
Обновление приложения		
-------------------------------------------------------------------------------

Подключить модуль:

	const publisher = require("encrypted-electron-publisher");

Запустить механизм проверки и закачки обновлений:

    publisher.updaterStart({
        platform: process.platform,
        update_interval: 300,
        remoteUrl: "http://update-server-host/builds/",
        cryptoAlgorithm: "aes-256-cbc",
        key: "0000000000000000000000000000000000000000000000000000000000000000",
        onClearData: null
    });
 
Параметры:
 - platform: платформа сборки (поддерживается win32 и linux) 
 - update_interval: интервал проверки обновлений на сервере
 - remoteUrl: url для проверки обновлений
 - cryptoAlgorithm: алгоритм шифрования. Данные пакет использует модуль шифрования Crypto. Об его алгоритмах шифрования можно узнать по ссылке [https://nodejs.org/api/crypto.html](https://nodejs.org/api/crypto.html)
 - key: ключ для дешифровки сборки по методы AES256
 - onClearData: событие для очистки данных при обновлении 

